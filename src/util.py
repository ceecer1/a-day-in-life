import json
import os
import time
import uuid
import base64

import boto3
from aws_lambda_powertools import Logger
from jinja2 import Environment
from jinja2_s3loader import S3loader

logger = Logger(child=True)


def validate_environment():
    try:
        os.environ["EMAIL_TEMPLATE_BUCKET_NAME"]
    except KeyError as ke:
        logger.error(
            "Please set the environment variables EMAIL_TEMPLATE_BUCKET_NAME"
        )
        raise ke


def is_valid_sqs(event):
    if "Records" in event and event["Records"]:
        logger.info("Valid SQS Message | Processing request")
    else:
        logger.info("Invalid SQS Message | Skipping request processing")
        return False
    validate_environment()
    return True


def generate_event_id():
    return str(uuid.uuid4())


def create_content(
    event_id,
    inquiry_type,
    template_prefix,
    first_name=None,
    last_name=None
):
    bucket_name = os.environ["EMAIL_TEMPLATE_BUCKET_NAME"]
    template_env = Environment(loader=S3loader(bucket_name, inquiry_type)) #bucketName/church_inquiry/NW8_template.html
    template_file = template_prefix + "_template.html"
    template = template_env.get_template(template_file)

    logger.info("Template file contents: {}".format(template))

    return template.render(first_name=first_name, last_name=last_name)


def create_response(status, message, event_id):
    logger.info("eventId: {} | status: {} | {}".format(event_id, status, message))
    return {
        "statusCode": status,
        "body": json.dumps({"message": message, "event_id": event_id}),
    }
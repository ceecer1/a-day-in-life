import json

from aws_lambda_powertools import Logger, Metrics, Tracer
import src.util as util

tracer = Tracer()
logger = Logger()
metrics = Metrics()


@metrics.log_metrics(capture_cold_start_metric=True)
@logger.inject_lambda_context
@tracer.capture_lambda_handler
def lambda_handler(event, context):
    if not util.is_valid_sqs(event):
        return {"statusCode": 200}

    for event in json.loads(json.dumps(event["Records"])):
        try:
            event_id = util.generate_event_id()
            body = json.loads(event["body"])
            post_code = body["postcode"].split(" ")[0]
            # template prefix must be either 'NW8' or 'decline'
            template_prefix = post_code if post_code == "NW8" else "decline"
            logger.info("Processing inquiry request messageId: {}".format(event["messageId"]))

            email_content = util.create_content(event_id, "church_inquiry", template_prefix,
                                                body["firstName"], body["lastName"])
            logger.info("Email content that is supposed to be sent via SES or other mechanism: {}".format(email_content))
            return util.create_response(200, "Processed inquiry", event_id)

        except RuntimeError as er:
            logger.error(er)
            logger.error(
                "event_id: {} | {} | messageId: {}".format(
                    event_id,
                    "Error processing inquiry request",
                    event["messageId"],
                )
            )
            raise
        except KeyError as er:
            logger.error(er)
            logger.error(
                "event_id: {} | {} | messageId: {}".format(
                    event_id,
                    "Error processing inquiry request",
                    event["messageId"],
                )
            )
            return util.create_response(
                400, getattr(er, "message", er.args[0]), event_id
            )

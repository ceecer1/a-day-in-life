import boto3
import os
from moto import mock_s3
from src import util
from src.util import create_content
from unittest import TestCase, mock

def apigw_event():
    return {
        "body": '{ "firstName": "John", "lastName": "Doe", "email": "johndoe@doe.com", "phone": "0400112233", "postcode": "NW8 9AY"}'
    }


class UtilTest(TestCase):
    def test_validate_environment(self):
        del os.environ["EMAIL_TEMPLATE_BUCKET_NAME"]
        try:
            util.validate_environment()
        except KeyError:
            pass
        else:
            self.fail("ExpectedException not raised")


    @mock_s3
    def test_create_content(self):
        bucket = "test_bucket"
        s3_client = boto3.client("s3")
        s3_client.create_bucket(
            Bucket=bucket,
            CreateBucketConfiguration={"LocationConstraint": "ap-southeast-2"},
        )

        s3_client.put_object(
            Bucket=bucket,
            Key="/church_inquiry/NW8_template.html",
            Body='<div> <p>Dear {{first_name}} {{last_name}}</p> <p>Thank you for your inquiry. Someone will be in touch with you shortly</p></div>',
        )

        response = create_content(
            "event_id",
            "church_inquiry",
            "NW8",
            first_name="John",
            last_name="Doe",
        )

        self.assertEqual(
            response,
            '<div> <p>Dear John Doe</p> <p>Thank you for your inquiry. Someone will be in touch with you shortly</p></div>',
        )
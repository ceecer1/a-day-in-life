import os
from uuid import uuid4

os.environ["AWS_XRAY_CONTEXT_MISSING"] = "LOG_ERROR"
os.environ["LAMBDA_TASK_ROOT"] = "true"
os.environ["POWERTOOLS_METRICS_NAMESPACE"] = "test"
os.environ["POWERTOOLS_SERVICE_NAME"] = "test"
os.environ["AWS_XRAY_LOG_LEVEL"] = "silent"

os.environ["EMAIL_TEMPLATE_BUCKET_NAME"] = "test_bucket"


class MockContext(object):
    def __init__(self, function_name):
        self.function_name = function_name
        self.function_version = "v$LATEST"
        self.memory_limit_in_mb = 512
        self.invoked_function_arn = (
            f"arn:aws:lambda:us-east-1:ACCOUNT:function:{self.function_name}"
        )
        self.aws_request_id = str(uuid4)


def lambda_context():
    return MockContext("dummy_function")
